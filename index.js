const express = require('express');

const app = express();

const port = 3000;

app.use(express.json());
app.use(express.urlencoded({extended: true}));

//A GET method request and response from an ExpressJS API
app.get('/home', (req, res) => {
	res.send('Welcome to the homepage')
});

//A GET method request and response from an ExpressJS API to retrieve data.
let users = [
	{
		"username": "johndoe",
		"password": "johndoe123"
	}
];

app.get('/users', (req, res) => {
	res.send(users)
});

//A DELETE method request and response from an ExpressJS API to delete a resource.
let message;

app.delete('/delete-user', (req, res) => {
	for(let i = 0; i < users.length; i++){
		if(req.body.username == users[i].username){
			users.splice(i, 1);
			message = `User ${req.body.username} has been deleted.`;
			break;
		} else {
			message = 'User does not exist.';
		}
	}
	res.send(message);
});

app.listen(port, () => console.log(`Server is running at port ${port}`));